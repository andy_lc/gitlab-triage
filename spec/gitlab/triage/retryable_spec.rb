require 'spec_helper'

require 'gitlab/triage/retryable'

describe Gitlab::Triage::Retryable do
  before do
    stub_const('RetryError', Class.new(StandardError))
    stub_const('AnotherRetryError', Class.new(StandardError))
    stub_const('BackoffError', Class.new(StandardError))
    stub_const('UnexpectedError', Class.new(StandardError))
    stub_const('Retryer', Class.new.include(described_class))
  end

  subject { Retryer.new }

  shared_examples 'exception handling' do
    context 'when an exception is raised' do
      describe 'default behavior' do
        it 'retries StandardError 3 times' do
          subject.execute_with_retry { raise error_to_raise }
        rescue error_to_raise
          expect(subject.tries).to eq(3)
        end
      end

      it 'retries the given exception MAX_RETRIES times' do
        subject.execute_with_retry(exception_types: exceptions, backoff_exceptions: backoff_exceptions) { raise error_to_raise }
      rescue error_to_raise
        expect(subject.tries).to eq(described_class::MAX_RETRIES)
      end

      it 'raises the final exception' do
        expect do
          subject.execute_with_retry(exception_types: exceptions, backoff_exceptions: backoff_exceptions) { raise error_to_raise }
        end.to raise_error(error_to_raise)
      end

      it 'ignores unexpected exception' do
        expect do
          subject.execute_with_retry(exception_types: exceptions, backoff_exceptions: backoff_exceptions) { raise UnexpectedError }
        end.to raise_error(UnexpectedError)
        expect(subject.tries).to eq(1)
      end
    end

    context 'when no exception is raised' do
      it 'returns the result' do
        result = subject.execute_with_retry { 5 }

        expect(result).to eq(5)
      end
    end
  end

  describe '#execute_with_retry' do
    let(:error_to_raise) { RetryError }
    let(:exceptions) { [] }
    let(:backoff_exceptions) { [] }

    context 'with one possible exception' do
      let(:exceptions) { error_to_raise }

      it_behaves_like 'exception handling'
    end

    context 'with multiple possible exceptions' do
      let(:exceptions) { [error_to_raise, AnotherRetryError] }

      it_behaves_like 'exception handling'

      context 'when second error occurs' do
        let(:error_to_raise) { AnotherRetryError }

        it_behaves_like 'exception handling'
      end
    end

    context 'with exceptions that does not need backoff' do
      let(:exceptions) { error_to_raise }
      let(:backoff_exceptions) { [] }

      it 'does not back off' do
        start_time = Time.now
        subject.execute_with_retry(exception_types: exceptions, backoff_exceptions: backoff_exceptions) { raise error_to_raise }
      rescue error_to_raise
        duration = Time.now - start_time
        expect(duration).to be < described_class::BACK_OFF_SECONDS
      end
    end

    context 'with exceptions needing backoff' do
      let(:error_to_raise) { BackoffError }
      let(:backoff_exceptions) { [error_to_raise] }

      before do
        stub_const('Gitlab::Triage::Retryable::BACK_OFF_SECONDS', 0.001)
      end

      it_behaves_like 'exception handling'

      it 'backs off at exceptions that need backoff' do
        start_time = Time.now
        subject.execute_with_retry(exception_types: exceptions, backoff_exceptions: backoff_exceptions) { raise error_to_raise }
      rescue error_to_raise
        duration = Time.now - start_time
        expect(duration).to be >= described_class::BACK_OFF_SECONDS * (described_class::MAX_RETRIES - 1)
      end
    end
  end
end
